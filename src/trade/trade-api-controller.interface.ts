import {GetTradesForRangeRequest, GetTradesForRangeResponse} from "./get";
import {Observable} from "rxjs";

export interface TradeApiControllerInterface {
    getTradesForRange(data: GetTradesForRangeRequest): Observable<GetTradesForRangeResponse>;
}