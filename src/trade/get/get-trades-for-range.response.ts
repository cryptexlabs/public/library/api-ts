import {TradeSerializedInterface} from "@cryptex-labs/common-trade";

export interface GetTradesForRangeResponse {
    data: TradeSerializedInterface[];
}