export interface GetTradesForRangeRequest {
    start: number;
    end: number;
}