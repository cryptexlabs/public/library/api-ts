import {CandleDataSerializedInterface} from "@cryptex-labs/common-candle/src/1.0.0/candle-data-serialized.interface";

export interface CandleUpdate {
    exchangeId: string;
    pairId: string;
    data: CandleDataSerializedInterface[];
}