import {CandleSubscription} from "./candle-subscription";

export interface SubscribeToCandleUpdatesRequest {
    subscriptions: CandleSubscription[];
    clientNumber: 1 | 2;
}