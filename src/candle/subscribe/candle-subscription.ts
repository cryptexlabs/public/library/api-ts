export interface CandleSubscription {
    exchangeId: string;
    pairId: string;
    granularities: number[];
}