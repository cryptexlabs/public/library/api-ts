import {CandleDataSerializedInterface} from "@cryptex-labs/common-candle";

export interface GetCandlesForGranularitiesAndRangeResponse {
    data: CandleDataSerializedInterface[];
}