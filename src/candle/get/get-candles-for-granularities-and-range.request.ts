export interface GetCandlesForGranularitiesAndRangeRequest {
    start: number;
    end: number;
    granularities: number[];
    exchangeId: string;
    pairId: string;
}