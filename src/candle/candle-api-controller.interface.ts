import {GetCandlesForGranularitiesAndRangeRequest, GetCandlesForGranularitiesAndRangeResponse} from "./get";
import {Observable} from "rxjs";
import {CandleUpdate, SubscribeToCandleUpdatesRequest} from "./subscribe";
import {Metadata} from "grpc";

export interface CandleApiControllerInterface {
    getCandlesForGranularitiesAndRange(
        request: GetCandlesForGranularitiesAndRangeRequest,
        metaData: Metadata): Observable<GetCandlesForGranularitiesAndRangeResponse>;

    subscribeToCandleUpdates(
        request: SubscribeToCandleUpdatesRequest): Observable<CandleUpdate>;
}