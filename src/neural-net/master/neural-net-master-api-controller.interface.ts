import {
    SubscribeToNextTrainingSessionRequest,
    SubscribeToSlaveStateUpdateRequestsRequest,
    TrainingSession
} from "./subscribe";
import {Observable} from "rxjs";
import {SlaveStateUpdate, UpdateTrainingSessionStatusRequest} from "./update";
import {SimpleResponse} from "./simple-response";

export interface NeuralNetMasterApiControllerInterface {
    subscribeToNextTrainingSession(request: SubscribeToNextTrainingSessionRequest): Observable<TrainingSession>;
    subscribeToSlaveStateUpdateRequests(request: SubscribeToSlaveStateUpdateRequestsRequest): Observable<SlaveStateUpdate>;
    updateTrainingSessionStatus(request: UpdateTrainingSessionStatusRequest): Observable<SimpleResponse>;
    updateSlaveState(request: SlaveStateUpdate): Observable<SimpleResponse>;
}