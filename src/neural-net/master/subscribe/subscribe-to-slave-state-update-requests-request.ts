export interface SubscribeToSlaveStateUpdateRequestsRequest {
    networkId: string;
    subscriptionId: string;
}