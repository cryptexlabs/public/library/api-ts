export interface SubscribeToNextTrainingSessionRequest {
    networkId: string;
    subscriptionId: string;
}