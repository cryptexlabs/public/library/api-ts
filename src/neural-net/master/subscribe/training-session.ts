export interface TrainingSession {
    learningRate: number;
    networkId: string;
    subscriptionId: string;
    sessionId: string;
    maxIterations: number;
    targetError: string;
    extra: any;
}