export interface UpdateTrainingSessionStatusRequest {
    iterationsCompleted: number;
    state: "in progress" | "complete" | "paused" | "error";
    description: string;
    secondsRemaining: number;
    currentError: number;
}